package com.example.onurcan.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class getLastUsers extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    public RecyclerView recyclerView;
    public Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_last_users);

        getUsers();
        Toast.makeText(context, "Kayıtların üzerine tıklayarak detayları görebilirsiniz", Toast.LENGTH_SHORT).show();
    }


    public void getUsers()
    {
        ApiHelper apiService = ApiClient.getClient().create(ApiHelper.class);
        Call<Answer> call = apiService.getUser();

        call.enqueue(new Callback<Answer>() {
            @Override
            public void onResponse(Call<Answer> call, Response<Answer> response){

                Detay detay = response.body().getDetay();
                List<User> user = detay.getUsers();


                for(User u : user)
                {

                    LinearLayout userLayout= new LinearLayout(context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 150);
                    params.setMargins(20,20,20,20);
                    params.gravity = Gravity.BOTTOM | Gravity.CENTER;
                    userLayout.setLayoutParams(params);
                    userLayout.setBackgroundResource(R.drawable.edittext_background);
                    userLayout.setOrientation(LinearLayout.HORIZONTAL);
                    userLayout.setGravity(View.TEXT_ALIGNMENT_GRAVITY);

                    final User gonderilen = u ;
                    userLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getLastUsers.this, Detail.class);
                            intent.putExtra("id",gonderilen.getId().toString());
                            intent.putExtra("ad",gonderilen.getAd());
                            intent.putExtra("soyad",gonderilen.getSoyad());
                            intent.putExtra("email",gonderilen.getEmail());
                            intent.putExtra("gsm",gonderilen.getGsm());
                            intent.putExtra("firmaId",gonderilen.getFirmaId());
                            intent.putExtra("toplamPuan",gonderilen.getSiparistoplampuan());
                            startActivity(intent);
                        }
                    });


                    TextView id = new TextView(context);
                    id.setTextColor(Color.BLACK);
                    id.setTextSize(20);
                    id.setText(u.getId().toString() + " - ");

                    TextView ad = new TextView(context);
                    ad.setTextColor(Color.BLACK);
                    ad.setTextSize(17);
                    if(u.getAd()=="")
                        ad.setText("(İsim yok)");
                    else
                        ad.setText(u.getAd());

                    userLayout.addView(id);
                    userLayout.addView(ad);


                    ((LinearLayout)findViewById(R.id.mainLayout)).addView(userLayout);
                }


            }

            @Override
            public void onFailure(Call<Answer> call, Throwable t) {Log.e(TAG, t.toString());
            }
        });

    }



}
