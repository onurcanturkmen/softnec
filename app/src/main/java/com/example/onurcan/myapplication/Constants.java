package com.example.onurcan.myapplication;

/**
 * Created by Onurcan on 27.04.2017.
 */

public class Constants {
    public static final String contentType = "x-www-form-urlencoded";
    public static final String email = "test_integration_user@soft-nec.com";
    public static final String password = "37&testPassword";
    public static final String BASE_URL = "http://95.173.179.99:8080/rest/api/v1/";
}
