package com.example.onurcan.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("sf_id")
    @Expose
    private Object sfId;
    @SerializedName("social_id")
    @Expose
    private Object socialId;
    @SerializedName("ad")
    @Expose
    private String ad;
    @SerializedName("soyad")
    @Expose
    private String soyad;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gsm")
    @Expose
    private Long gsm;
    @SerializedName("kurumsal")
    @Expose
    private Long kurumsal;
    @SerializedName("firma_id")
    @Expose
    private Long firmaId;
    @SerializedName("il_id")
    @Expose
    private Object ilId;
    @SerializedName("ilce_id")
    @Expose
    private Object ilceId;
    @SerializedName("semt_id")
    @Expose
    private Object semtId;
    @SerializedName("acikadres")
    @Expose
    private String acikadres;
    @SerializedName("dogumtarih")
    @Expose
    private Object dogumtarih;
    @SerializedName("cinsiyet")
    @Expose
    private Long cinsiyet;
    @SerializedName("iskonto")
    @Expose
    private String iskonto;
    @SerializedName("publicpuan")
    @Expose
    private Long publicpuan;
    @SerializedName("siparistoplampuan")
    @Expose
    private Long siparistoplampuan;
    @SerializedName("kalantoplamborcu")
    @Expose
    private Long kalantoplamborcu;
    @SerializedName("misafirmi")
    @Expose
    private Long misafirmi;
    @SerializedName("bildirim_email")
    @Expose
    private Long bildirimEmail;
    @SerializedName("bildirim_sms")
    @Expose
    private Long bildirimSms;
    @SerializedName("aktif")
    @Expose
    private Long aktif;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    User()
    {
        super();
    }
    User(String ad, String soyad, String email)
    {
        this.ad = ad;
        this.soyad = soyad;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Object getSfId() {
        return sfId;
    }

    public void setSfId(Object sfId) {
        this.sfId = sfId;
    }

    public Object getSocialId() {
        return socialId;
    }

    public void setSocialId(Object socialId) {
        this.socialId = socialId;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getGsm() {
        return gsm;
    }

    public void setGsm(Long gsm) {
        this.gsm = gsm;
    }

    public Long getKurumsal() {
        return kurumsal;
    }

    public void setKurumsal(Long kurumsal) {
        this.kurumsal = kurumsal;
    }

    public Long getFirmaId() {
        return firmaId;
    }

    public void setFirmaId(Long firmaId) {
        this.firmaId = firmaId;
    }

    public Object getIlId() {
        return ilId;
    }

    public void setIlId(Object ilId) {
        this.ilId = ilId;
    }

    public Object getIlceId() {
        return ilceId;
    }

    public void setIlceId(Object ilceId) {
        this.ilceId = ilceId;
    }

    public Object getSemtId() {
        return semtId;
    }

    public void setSemtId(Object semtId) {
        this.semtId = semtId;
    }

    public String getAcikadres() {
        return acikadres;
    }

    public void setAcikadres(String acikadres) {
        this.acikadres = acikadres;
    }

    public Object getDogumtarih() {
        return dogumtarih;
    }

    public void setDogumtarih(Object dogumtarih) {
        this.dogumtarih = dogumtarih;
    }

    public Long getCinsiyet() {
        return cinsiyet;
    }

    public void setCinsiyet(Long cinsiyet) {
        this.cinsiyet = cinsiyet;
    }

    public String getIskonto() {
        return iskonto;
    }

    public void setIskonto(String iskonto) {
        this.iskonto = iskonto;
    }

    public Long getPublicpuan() {
        return publicpuan;
    }

    public void setPublicpuan(Long publicpuan) {
        this.publicpuan = publicpuan;
    }

    public Long getSiparistoplampuan() {
        return siparistoplampuan;
    }

    public void setSiparistoplampuan(Long siparistoplampuan) {
        this.siparistoplampuan = siparistoplampuan;
    }

    public Long getKalantoplamborcu() {
        return kalantoplamborcu;
    }

    public void setKalantoplamborcu(Long kalantoplamborcu) {
        this.kalantoplamborcu = kalantoplamborcu;
    }

    public Long getMisafirmi() {
        return misafirmi;
    }

    public void setMisafirmi(Long misafirmi) {
        this.misafirmi = misafirmi;
    }

    public Long getBildirimEmail() {
        return bildirimEmail;
    }

    public void setBildirimEmail(Long bildirimEmail) {
        this.bildirimEmail = bildirimEmail;
    }

    public Long getBildirimSms() {
        return bildirimSms;
    }

    public void setBildirimSms(Long bildirimSms) {
        this.bildirimSms = bildirimSms;
    }

    public Long getAktif() {
        return aktif;
    }

    public void setAktif(Long aktif) {
        this.aktif = aktif;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}