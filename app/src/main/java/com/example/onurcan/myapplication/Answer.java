package com.example.onurcan.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class Answer {
    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("Detay")
    @Expose
    private Detay detay;

    Answer()
    {
        super();
        System.out.println(result);
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Detay getDetay() {
        return detay;
    }

    public void setDetay(Detay detay) {
        this.detay = detay;
    }
}