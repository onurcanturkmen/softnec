package com.example.onurcan.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Onurcan on 27.04.2017.
 */

public class Result {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("Detay")
    @Expose
    private String detay;
    @SerializedName("id")
    @Expose
    private Long id;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDetay() {
        return detay;
    }

    public void setDetay(String detay) {
        this.detay = detay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
