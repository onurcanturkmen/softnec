package com.example.onurcan.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Detail extends AppCompatActivity {

    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();

        TextView txt = new TextView(context);
        txt.setText("Id : " + intent.getStringExtra("id")
                + "\nAd : " + intent.getStringExtra("ad")
                + "\nSoyad : " + intent.getStringExtra("soyad")
                + "\nEmail : " + intent.getStringExtra("email")
                + "\nGSM : " + intent.getStringExtra("gsm")
                + "\nFirma Id : " + intent.getStringExtra("firmaId")
                + "\nToplam Puan : " + intent.getStringExtra("toplamPuan")
        );
        txt.setBackgroundResource(R.drawable.edittext_background);
        txt.setTextColor(Color.BLACK);
        txt.setTextSize(20);
        txt.setPadding(75,75,75,75);

        ((LinearLayout)findViewById(R.id.mainLayout)).addView(txt);

    }


}
