package com.example.onurcan.myapplication;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface ApiHelper {
    @Headers({
            "content-type: " + Constants.contentType,
            "email: " + Constants.email,
            "password: " + Constants.password
    })
    @POST("user")
    Call<Answer> addUser(@Body User data);

    @Headers({
            "Content-Type: " + Constants.contentType,
            "email: " + Constants.email,
            "password: " + Constants.password
    })
    @GET("user")
    Call<Answer> getUser();
}
