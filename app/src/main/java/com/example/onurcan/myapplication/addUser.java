package com.example.onurcan.myapplication;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class addUser extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
    }

    public void addUser(View view)
    {
        if(!isOnline())
        {
            Toast.makeText(this, "İnternet bağlantınızı kontrol ediniz", Toast.LENGTH_SHORT).show();
            return;
        }
        String name = ((EditText)findViewById(R.id.name)).getText().toString();
        String lastname = ((EditText)findViewById(R.id.lastname)).getText().toString();
        String email = ((EditText)findViewById(R.id.email)).getText().toString();

        if(!isValidEmail(email))
        {
            Toast.makeText(addUser.this, "Lütfen geçerli bir mail adresi giriniz.", Toast.LENGTH_SHORT).show();
            return;
        }

        User user = new User(name,lastname,email);

        ApiHelper apiHelper = ApiClient.getClient().create(ApiHelper.class);
        Call<Answer> call = apiHelper.addUser(user);

        call.enqueue(new Callback<Answer>() {
            @Override
            public void onResponse(Call<Answer> call, Response<Answer> response) {

                if(response.body()==null) {
                    Toast.makeText(context, "Kayıt yapılamadı", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(context, "Yeni kayıt oluşturuldu", Toast.LENGTH_SHORT).show();
                return;
            }
                @Override
                public void onFailure(Call<Answer> call, Throwable t) {
                    Log.e(TAG, t.toString());
                }
            });
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
