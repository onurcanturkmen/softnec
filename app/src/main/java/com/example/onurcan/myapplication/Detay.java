package com.example.onurcan.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;


public class Detay {
    @SerializedName("Toplam")
    @Expose
    private Integer toplam;
    @SerializedName("Gosterilen")
    @Expose
    private Integer gosterilen;
    @SerializedName("Users")
    @Expose
    private List<User> users = null;

    public Detay(Integer toplam, Integer gosterilen, List<User> users) {
        this.toplam = toplam;
        this.gosterilen = gosterilen;
        this.users = users;
    }

    public Integer getToplam() {
        return toplam;
    }

    public void setToplam(Integer toplam) {
        this.toplam = toplam;
    }

    public Integer getGosterilen() {
        return gosterilen;
    }

    public void setGosterilen(Integer gosterilen) {
        this.gosterilen = gosterilen;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}